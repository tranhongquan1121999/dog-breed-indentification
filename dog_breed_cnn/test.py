import cv2
from keras.models import load_model
import numpy as np
import matplotlib.pyplot as plt

list_of_dog = ["boston bull", "Chihuahua", "doberman", "golden_retriever", "irish_wolfhound", "redbone"]

model = load_model('dogmodel1.hdf5')

img = cv2.imread("2.jpg")

img1 = cv2.resize(img,(299,299))

img1 = np.reshape(img1,[1,299,299,3])

y_pred = model.predict(img1)

arr = y_pred[0]

index = np.where(arr == np.amax(arr))

dog_breed = list_of_dog[int(index[0])]

print("This dog breed is: ", dog_breed )


cv2.putText(img,dog_breed, (30,30), cv2.FONT_HERSHEY_SIMPLEX, 1.5 , (255,0,0), 4)
plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
plt.show()













